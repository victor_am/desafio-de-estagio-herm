# Desafio de estágio - Valeu App

O desafio contará com um sistema de gamificacão, ou seja, cada funcionalidade receberá uma pontuacão e o objetivo será alcancar o maior número de pontos possível.  

Deverá ser criada uma lista de pokemons (PokeDex) de acordo com as seguintes especificacões:

- O projeto deverá ser realizado em React JS; 
- Deverá ser consumida a seguinte API: https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json
- O projeto deverá estar no github ou gitlab e o link deverá ser enviado para o e-mail: contato@valeuapp.com.br
- Prazo de envio máximo será até dia 13/06/2021 ás 23:59.

# Gamificacão

- Reproducão do layout disponibilizado: 100 pontos
- Layout responsivo : 50 pontos
- funcionalidade de pesquisar pokemons: 50 pontos
- funcionalidade de filtrar por tipo de pokemon: 50 pontos
- Utilizar Typescript no projeto: 50 pontos


# Sobre o layout

**Você pode acessar o layout através desse link:** https://xd.adobe.com/view/8f552dc0-92e5-44a4-8632-0d5d036670be-da06/

